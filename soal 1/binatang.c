#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <dirent.h>
#include <string.h>
#include <time.h>

int main() {
  char *arr_folder[] = {"HewanDarat", "HewanAmphibi", "HewanAir"}; 
  char *arr_file[] = {"darat", "amphibi", "air"}; 
  char destination[] = "/Users/anggarasaputra/Desktop/Kodingan/Semester 4/Sisop/Praktikum 2/Soal 1";

  // execl("/bin/pwd", "pwd", NULL);
  pid_t child_id;
  int status;

  if((child_id = fork()) == 0) {
  char *argv[] = {"wget", "-q","-O", "binatang.zip", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", NULL};
      execv("/opt/local/bin/wget", argv);
      exit(0);
  }
  wait(&status);

  if((child_id = fork()) == 0) {
    char *argv[] = {"unzip", "binatang.zip", "-d", "./binatangdir", NULL};
    execv("/usr/bin/unzip", argv);
  }
  wait(&status);

 // random pick photo from binatangdir folder
  DIR* dir;
  struct dirent *entry;
  char* path = "./binatangdir";
  int count = 0;

  // open the directory
    dir = opendir(path);
    if (dir == NULL) {
        perror("opendir");
        exit(EXIT_FAILURE);
    }
    
    // count the number of files in the directory
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) { // regular file
            count++;
        }
    }
    
    // close the directory and reopen it
    closedir(dir);
    dir = opendir(path);
    
    // generate a random number between 0 and count-1
    srand(time(NULL)); // seed the random number generator
    int random_num = rand() % count;
    
    // iterate through the directory until the randomly selected file is found
    count = 0;
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) { // regular file
            if (count == random_num) {
                // print the file name
                printf("Randomly selected file: %s\n", entry->d_name);
                break;
            }
            count++;
        }
    }
    closedir(dir);

  for(int i = 0; i<3; i++) {
  child_id = fork();
  if (child_id < 0) {
    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  }
  child_id = fork();
  if (child_id == 0) {
    // this is child
    pid_t files = fork();
    if(files == 0){
    char *argv[] = {"mkdir", arr_folder[0], arr_folder[1], arr_folder[2], NULL};
    execv("/bin/mkdir", argv);
    }
    wait(&status);

    DIR *dir;
    struct dirent *ent;
    char *source_dir = "binatangdir";
    char *filename;
    char source_path[100];
    char dest_path[100];

    if ((dir = opendir("./binatangdir")) != NULL) {
        while ((ent = readdir(dir)) != NULL) {
            if (ent->d_type == DT_REG) {  // check if the entry is a regular file
                filename = ent->d_name;
                for (int i = 0; i < 3; i++) {  // loop through the arr_file array
                    if (strstr(filename, arr_file[i]) != NULL) {  // check if the filename contains arr_file[i]
                        sprintf(source_path, "binatangdir/%s", filename);
                        sprintf(dest_path, "%s", arr_folder[i]);
                        char *argv[] = {"mv", source_path, dest_path};
                        printf("%s\n", source_path);
                        printf("%s\n", dest_path);
                        pid_t moves = fork();
                        if(moves == 0) {
                        execl("/bin/mv", "mv", source_path, dest_path,  NULL);  // move the file to the destination folder
                        }
                    }
                }
            }
        }
        closedir(dir);
    } else {
        perror("Error opening directory");
        return EXIT_FAILURE;
    }
    pid_t cZip;
    cZip = fork();

  if ((cZip = fork()) == 0) {
    char *argv[] = {"zip", "-r", destination,"HewanDarat", NULL};
    execv("/usr/bin/zip", argv);
    exit(0);
  }
  wait(NULL);

  if ((cZip = fork()) == 0) {
    char *argv[] = {"zip", "-r", destination,"HewanAmphibi", NULL};
    execv("/usr/bin/zip", argv);
    exit(0);
  }
  wait(NULL);

  if ((cZip = fork()) == 0) {
    char *argv[] = {"zip", "-r", destination,"HewanAir", NULL};
    execv("/usr/bin/zip", argv);
    exit(0);
  }
  }
  exit(EXIT_SUCCESS);
}
 }

