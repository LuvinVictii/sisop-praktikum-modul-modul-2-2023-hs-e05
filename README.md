# sisop-praktikum-modul-modul-2-2023-HS-E05

## Soal 1 

### Penjelasan
Program ini berjalan dengan mendownload terlebih dahulu file yang berisikan foto-foto binatang dari url yang ada.
Kemudian unzip dan select salah satu file foto secara acak.

Setelah itu membuat 3 file yaitu HewanAmphibi, HewanAir, dan HewanDarat untuk memindahkan file .jpg ke 3 folder tersebut sesuai dengan jenis mereka.Lalu melakukan zip kembali untuk folder tersebut.

### Cara Pengerjaan
1. download terlebih dahulu file zip dan kemudian unzip
```
  if((child_id = fork()) == 0) {
  char *argv[] = {"wget", "-q","-O", "binatang.zip", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", NULL};
      execv("/opt/local/bin/wget", argv);
      exit(0);
  }
  wait(&status);

  if((child_id = fork()) == 0) {
    char *argv[] = {"unzip", "binatang.zip", "-d", "./binatangdir", NULL};
    execv("/usr/bin/unzip", argv);
  }
  wait(&status);
  ```
2. Random pick salah satu gambar dari file yang sudah di unzip
```
DIR* dir;
  struct dirent *entry;
  char* path = "./binatangdir";
  int count = 0;

  // open the directory
    dir = opendir(path);
    if (dir == NULL) {
        perror("opendir");
        exit(EXIT_FAILURE);
    }
    
    // count the number of files in the directory
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) { // regular file
            count++;
        }
    }
    
    // close the directory and reopen it
    closedir(dir);
    dir = opendir(path);
    
    // generate a random number between 0 and count-1
    srand(time(NULL)); // seed the random number generator
    int random_num = rand() % count;
    
    // iterate through the directory until the randomly selected file is found
    count = 0;
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) { // regular file
            if (count == random_num) {
                // print the file name
                printf("Randomly selected file: %s\n", entry->d_name);
                break;
            }
            count++;
        }
    }
    closedir(dir);
```
Hasil :
![Random pick picture](soal 1/Jepretan_Layar_2023-04-08_pukul_18.39.36.png)

3. Bagi setiap gambar yang ada di file binatang berdasarkan tipenya kemudian lakukan zip

![3 File & zip](soal 1/Jepretan_Layar_2023-04-08_pukul_18.40.11.png)


### Kesulitan
Belum berhasil menjalankan perintah zip untuk potongan kode tersebut :

![Nothing to do Zip](soal 1/Jepretan_Layar_2023-03-25_pukul_21.41.15.png)

### Fix

![Fix Zip](soal 1/Jepretan_Layar_2023-04-08_pukul_18.40.26.png)


## Soal 2

### Penjelasan
Fungsi start digunakan untuk membuat daemon process yang akan menjalankan program secara terus-menerus dengan menggunakan perulangan while. Proses ini akan memanggil fungsi program untuk membuat folder baru, mengunduh gambar, dan menghapus folder yang telah diunduh dan dikompres.

Fungsi program akan membuat folder baru dengan timestamp sebagai nama folder, mengunduh 15 gambar, mengkompres folder, dan menghapus folder, serta membuat gambar yang diunduh menyesuaikan ukurannya sesuai waktu diunduh

Fungsi generateKiller digunakan untuk membuat file "killer" yang dapat membunuh semua proses dengan nama "killer" yang sedang berjalan atau hanya anak-anak dari proses "killer" menggunakan opsi -a dan -b pada command line argument.

### Kesulitan
Saya mendapatkan error ini
![Error line 12](https://media.discordapp.net/attachments/883545778533056583/1089177014558277662/image.png?width=1047&height=103)

Sedangkan line 12 saya adalah
![Line 12](https://media.discordapp.net/attachments/883545778533056583/1089177050427949116/image.png?width=604&height=138)

### Fix
Saya salah cara menjalankan kode. Sudah dicoba dengan cara yang benar dan mendapatkan hasil yang sesuai


## Soal 3
### Penjelasan
Penyelesaian soal ini menggunakan `exec()` dan `fork()`. Dimana di dalamnya terdapat beberapa perintah yaitu download, unzip, dan delete file. Kemudian, isi dari file tersebut akan dikelompokkan menjadi beberapa folder tiap posisinya. Lalu, dibuat fungsi buatTim yang teridiri dari bek, gelandang, dan striker. 

### Cara Pengerjaan 

1. Deklarasikan struct Player.
2. Deklarasikan fungsi-fungsi untuk menjalankan program yaitu `downloadDatabase()`, `extractPlayers()`, `removeNonMUPlayers()`, `categorizePlayers()`, dan `buatTim`.
3. Program ini menggunakan `fork()` untuk membuat proses baru. `fork()` akan mengembalikan nilai 0 untuk proses child dan pid dari child process untuk parent process.
4. Dalam fungsi `downloadDatabase()`, proses download dilakukan menggunakan program wget melalui fungsi `execlp()`. Setelah itu, fungsi akan mencetak pesan "Download complete." ke layar. Namun, karena `execlp()` akan menggantikan proses yang sedang berjalan dengan program wget, maka perintah `printf()` setelah `execlp()` tidak akan pernah dieksekusi. Sehingga pesan "Download complete." tidak akan ditampilkan di layar.
5. Dalam fungsi `extractPlayers()` file unduhan dari drive akan di-_extract_. Kemudian, file zip akan dihapus. 
6. Fungsi `removeNonMUPlayers()` digunakan untuk menghapus pemain-pemain yang bukan dari MU. Fungsi membuka direktori `/home/shafanh` menggunakan `opendir()`. Fungsi mendeklarasikan variabel `entry` dengan tipe `struct dirent` yang akan digunakan untuk membaca entri direktori satu per satu.  `readdir()` digunakan untuk membaca entri direktori yang belum dibaca. Jika tidak ada entri lagi, fungsi akan keluar dari loop.Jika tipe entri adalah file regular, maka pemrosesan akan dilanjutkan. Jika bukan file regular, pemrosesan tidak dilakukan. Fungsi `filename` mendeklarasikan variabel filename yang menyimpan nama file entri direktori yang sedang diproses. Fungsi `token` mendeklarasikan variabel token yang akan digunakan untuk memisahkan nama file menjadi token-token yang terpisah dengan delimiter "_". Fungsi mendeklarasikan variabel `count` yang akan digunakan untuk menghitung token-token yang terpisah dari nama file. `strtok()` digunakan untuk memisahkan nama file menjadi token. Untuk membandingkan nama klub, makan gunakan `strcmp()`. Lalu `remove()` untuk menghapus file dengan nama `filename`.
7. Fungsi `categorizePlayers()` digunakan untuk membaca nama-nama file dalam direktori kemudian dikategorikan berdasarkan rating. Dalam loop , tiap file yang ditemukan akan dipecah-pecahkan menjadi beberapa bagian yang dipisahkan oleh karakter underscore "_" menggunakan fungsi `strtok()`. Setiap bagian tersebut akan ditampung ke dalam variabel `players` dengan menggunakan indeks `numPlayers` yang digunakan untuk menghitung jumlah pemain yang telah ditambahkan. Setelah itu, rating pemain akan diambil dari bagian terakhir nama file, dan akan digunakan untuk mengkategorikan pemain tersebut. Setelah dikategorikan, nama file pemain akan dipindahkan ke direktori yang sesuai dengan kategori mereka dengan menggunakan fungsi `rename()`.
8. Fungsi `buatTim()` digunakan untuk membuat susunan tim dengan jumlah pemain di setiap posisi yang sudah ditentukan (bek, gelandang, dan striker). Pertama, fungsi ini akan memisahkan indeks pemain untuk setiap posisi yang sudah ada dalam struct `players`. Selanjutnya, fungsi akan memeriksa apakah jumlah pemain di setiap posisi sudah cukup untuk membentuk tim. Jika jumlah pemain belum cukup, maka akan dikeluarkan pesan "Tidak cukup pemain untuk membentuk tim." dan fungsi akan diakhiri. Jika sudah cukup, fungsi akan mengacak indeks pemain untuk setiap posisi dan membangun susunan tim sesuai dengan jumlah pemain di setiap posisi yang telah ditentukan.


Hasil output:
- Download file
![download](soal 3/download.png)
- Extract file

![extract](soal 3/extract.png)
- Isi file players
![players](soal 3/players.png)

### Kesulitan
Kesulitan yang dialami adalah saat mengkategorisasikan dan membuat susunan tim. Sering kali terjadi error saat mendefinisikan fungsinya.

### Fix
Sudah berhasil melengkapi fungsi `categorizePlayers()` dan `buatTim()` dimulai dari line 99 hingga akhir.

## Soal 4

### Penjelasan
Membuat sebuah program daemon yang dimana menerima config untuk 5 argument dan menampilkan pesan error, serta tentu berjalan di bacground dan mengambil 1 config cron

### Cara Pengerjaan
1. Memeriksa argumen yang telah di parsing tersebut merupakan asterik atau bukan
```
#define MAX_ARG_COUNT 4

int main(int argc, char *argv[]) {
    if (argc != MAX_ARG_COUNT + 1) {
        fprintf(stderr, "Invalid number of arguments\n");
        exit(EXIT_FAILURE);
    }

    bool secondAsterisk = strcmp(argv[1], "*") == 0;
    bool minuteAsterisk = strcmp(argv[2], "*") == 0;
    bool hourAsterisk = strcmp(argv[3], "*") == 0;
    int convertedOne = atoi(argv[1]);
    int convertedTwo = atoi(argv[2]);
    int convertedThree = atoi(argv[3]);
    printf("Converted arguments: %d, %d, %d\n", convertedOne, convertedTwo, convertedThree);
```
2. mengecek rentang argumen dan tetapkan argumen terakhir jalur skrip yang akan dijalankan di dalam proses daemon
```
    if (convertedOne < 0 || convertedOne > 59 || convertedTwo < 0 || convertedTwo > 59 || convertedThree < 0 || convertedThree > 23) {
        fprintf(stderr, "Invalid argument values\n");
        exit(EXIT_FAILURE);
    }

    char *args[] = {"bash", argv[4], NULL};

```
3. Daemon process
```
pid_t pid = fork();
    if (pid < 0) {
        fprintf(stderr, "Failed to fork\n");
        exit(EXIT_FAILURE);
    } else if (pid > 0) {
        // Parent process - exit
        printf("Daemon process started with PID %d\n", pid);
        exit(EXIT_SUCCESS);
    }

    if (setsid() < 0) {
        fprintf(stderr, "Failed to create a new session\n");
        exit(EXIT_FAILURE);
    }

    umask(0);
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    while (true) {
        time_t now = time(NULL);
        struct tm *local_time = localtime(&now);
        int second = local_time->tm_sec;
        int minute = local_time->tm_min;
        int hour = local_time->tm_hour;

        if ((secondAsterisk || second == convertedOne) && (minuteAsterisk || minute == convertedTwo) && (hourAsterisk || hour == convertedThree)) {
            pid_t child_pid = fork();
            if (child_pid == 0) {
                execv("/bin/bash", args);
                fprintf(stderr, "Failed to execute command\n");
                exit(EXIT_FAILURE);
            } else if (child_pid < 0) {
                fprintf(stderr, "Failed to fork\n");
                exit(EXIT_FAILURE);
            } else {
                wait(NULL);
            }
        }

        // Sleep for 1 second
        sleep(1);
    }
```

4. buat cron.sh 
```
# cron.sh 
echo "Time at $(date)" >> daemon.txt
```
5. config running dan result
```
./mainan \* \* \* ./cron.sh
```
Terminal:
![foto4.1](soal 4/Jepretan_Layar_2023-04-08_pukul_21.07.39.png)
Status CPU: 
![foto4.2](soal 4/Jepretan_Layar_2023-04-08_pukul_21.10.07.png)


