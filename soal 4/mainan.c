#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>

#define MAX_ARG_COUNT 4

int main(int argc, char *argv[]) {
    // Check if the number of arguments is valid
    if (argc != MAX_ARG_COUNT + 1) {
        fprintf(stderr, "Invalid number of arguments\n");
        exit(EXIT_FAILURE);
    }

    // Parse command line arguments
    bool secondAsterisk = strcmp(argv[1], "*") == 0;
    bool minuteAsterisk = strcmp(argv[2], "*") == 0;
    bool hourAsterisk = strcmp(argv[3], "*") == 0;
    int convertedOne = atoi(argv[1]);
    int convertedTwo = atoi(argv[2]);
    int convertedThree = atoi(argv[3]);
    printf("Converted arguments: %d, %d, %d\n", convertedOne, convertedTwo, convertedThree);

    // Validate command line arguments
    if (convertedOne < 0 || convertedOne > 59 || convertedTwo < 0 || convertedTwo > 59 || convertedThree < 0 || convertedThree > 23) {
        fprintf(stderr, "Invalid argument values\n");
        exit(EXIT_FAILURE);
    }

    // Store the path in a vector of arguments
    char *args[] = {"bash", argv[4], NULL};

    // Daemonization
    pid_t pid = fork();
    if (pid < 0) {
        fprintf(stderr, "Failed to fork\n");
        exit(EXIT_FAILURE);
    } else if (pid > 0) {
        // Parent process - exit
        printf("Daemon process started with PID %d\n", pid);
        exit(EXIT_SUCCESS);
    }

    // Child process
    if (setsid() < 0) {
        fprintf(stderr, "Failed to create a new session\n");
        exit(EXIT_FAILURE);
    }

    umask(0);
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    // Daemon loop
    while (true) {
        time_t now = time(NULL);
        struct tm *local_time = localtime(&now);
        int second = local_time->tm_sec;
        int minute = local_time->tm_min;
        int hour = local_time->tm_hour;

        if ((secondAsterisk || second == convertedOne) && (minuteAsterisk || minute == convertedTwo) && (hourAsterisk || hour == convertedThree)) {
            // Create a new process to execute the command
            pid_t child_pid = fork();
            if (child_pid == 0) {
                execv("/bin/bash", args);
                fprintf(stderr, "Failed to execute command\n");
                exit(EXIT_FAILURE);
            } else if (child_pid < 0) {
                fprintf(stderr, "Failed to fork\n");
                exit(EXIT_FAILURE);
            } else {
                // Wait for the child process to finish
                wait(NULL);
            }
        }

        // Sleep for 1 second
        sleep(1);
    }

    // Unreachable code
    return 0;
}