#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>

void start(char*, int);
void generateKiller(int, int);
void program();

int main(int argc, char *argv[]) {
  char cwd[4096];
  if (getcwd(cwd, sizeof(cwd)) == NULL) exit(EXIT_FAILURE);

  int option;
  while ((option = getopt(argc, argv, "ab")) != -1) {
    switch (option) {
      case 'a':
        start(cwd, 0);
        break;
      case 'b':
        start(cwd, 1);
        break;
      case '?':
        printf("Options available:\n    -a Stops all processes with the `killer` program\n    -b Stops all child processes with the `killer` program\n");
        break;
    }
  }
}

void start(char *cwd, int mode) {
  pid_t pid, sid;

  pid = fork();

  if (pid < 0) {
    exit(EXIT_FAILURE);
  } 
  if (pid > 0) {
    // Generate killer
    generateKiller(mode, pid);
    exit(EXIT_SUCCESS);
  }
  
  umask(0);

  sid = setsid();
  if (sid < 0) exit(EXIT_FAILURE);

  if ((chdir(cwd)) < 0) exit(EXIT_FAILURE);

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

  while (1) {
    pid_t ppid;
    int status;

    ppid = fork();

    if (ppid == 0) {
      program();
      exit(EXIT_SUCCESS);
    }

    sleep(5);
  }
}

void program() {
  pid_t pid;
  int status;
  
  time_t currentTime = time(NULL);
  struct tm *timestamp = localtime(&currentTime);
  
  char folderPath[64];
  strftime(folderPath, sizeof(folderPath), "./%Y-%m-%d_%H:%M:%S", timestamp);

  pid = fork();
  if (pid == 0) {
    char *argv[] = {"mkdir", folderPath, NULL};
    execv("/bin/mkdir", argv);
  } else {
    wait(&status);
  }

  for (int i = 0; i < 15; i++) {
    pid = fork();

    if (pid == 0) {
      currentTime = time(NULL);
      timestamp = localtime(&currentTime);

      char url[32];
      snprintf(url, sizeof(url), "https://picsum.photos/%ld", (currentTime % 1000) + 50);

      char timestampStr[32];
      strftime(timestampStr, sizeof(timestampStr), "%Y-%m-%d_%H:%M:%S.png", timestamp);

      char filePath[4096];
      snprintf(filePath, sizeof(filePath), "./%s/%s", folderPath, timestampStr);

      char *argv[] = {"wget", "-q", "-P", folderPath, "-O", filePath, url, NULL};
      execv("/bin/wget", argv);
    }

    sleep(1);
  }

  pid = fork();
  if (pid == 0) {
    char *argv[] = {"zip", "-q", "-r", folderPath, folderPath, NULL};
    execv("/bin/zip", argv);
  } else {
    wait(&status);
  }

  pid = fork();
  if (pid == 0) {
    char *argv[] = {"rm", "-rf", folderPath, NULL};
    execv("/bin/rm", argv);
  } else {
    wait(&status);
  }
}

void generateKiller(int mode, int pid) {
  char program[64];
  if (mode == 0) {
    snprintf(program, sizeof(program), "pkill -9 -P %d\nkill -9 %d\nrm killer", pid, pid);
  } else if (mode == 1) {
    snprintf(program, sizeof(program), "kill -9 %d\nrm killer", pid);
  }

  FILE *fp = fopen("killer", "w");
  fprintf(fp, "%s", program);
  fflush(fp);
  fclose(fp);

  pid_t ppid = fork();
  if (ppid == 0) {
    char *argv[] = {"chmod", "+x", "killer", NULL};
    execv("/bin/chmod", argv);
  }
}
