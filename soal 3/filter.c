#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/wait.h>

#define MAX_PLAYERS 100
#define MAX_NAME_LEN 50

typedef struct Player {
    char name[MAX_NAME_LEN];
    char team[MAX_NAME_LEN];
    char position[MAX_NAME_LEN];
    int rating;
} Player;

void downloadDatabase();
void extractPlayers();
void removeNonMUPlayers();
void categorizePlayers();
void buatTim(int bek, int gelandang, int striker);

int numPlayers = 0;
Player players[MAX_PLAYERS];

int main() {
    pid_t pid = fork();
    if (pid == 0){
        downloadDatabase();
    } else if (pid > 0){
        wait(NULL);
        extractPlayers();
        categorizePlayers();
        removeNonMUPlayers();
        buatTim(5, 4, 1);
    } else {
        perror("fork");
        exit(1);
    }
    return 0;
}

void downloadDatabase() {
    printf("Downloading database...\n");
    execlp("/usr/bin/wget", "wget", "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download", "-O", "players.zip", NULL);
    printf("Download complete.\n");
}

void extractPlayers() {
    printf("Extracting players...\n");
    pid_t pid = fork();
    if (pid == 0) {
        if (execlp("unzip", "unzip", "../players.zip", NULL) == -1) {
            perror("unzip");
            exit(1);
        }
    } else {
        wait(NULL);
        printf("Extraction complete.\n");
        pid_t pid2 = fork();
        if (pid2 == 0) {
            if (execlp("rm", "rm", "../players.zip", NULL) == -1) {
                perror("rm");
                exit(1);
            }
        } else {
            wait(NULL);
            printf("Zip file removed.\n");
        }
    }
}

void removeNonMUPlayers() {
    printf("Removing non-Manchester United players...\n");
    DIR* dir = opendir("/home/shafanh/modul2");
    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
            char* filename = entry->d_name;
            char* token = strtok(filename, "_");
            int count = 0;
            while (token != NULL) {
                count++;
                if (count == 2) {
                    if (strcmp(token, "ManchesterUnited") != 0) {
                        remove(filename);
                        printf("Removed player %s.\n", filename);
                    }
                }
                token = strtok(NULL, "_");
            }
        }
    }
    closedir(dir);
    printf("Non-Manchester United players removed.\n");
}

void categorizePlayers() {
        printf("Categorizing players...\n");
        DIR* dir = opendir("/home/shafanh/modul2");
        struct dirent *entry;
        while ((entry = readdir(dir)) != NULL) {
                if (entry->d_type == DT_REG) {
                        char *filename = entry->d_name;
                        char* token = strtok(filename, "_");
                        int count = 0;
                        while (token != NULL) {
                                count++;
                                if (count == 1) {
                                        strcpy(players[numPlayers].name, token);
                                } else if (count == 2) {
                                        strcpy(players[numPlayers].team, token);
                                } else if (count == 3) {
                                        strcpy(players[numPlayers].position, token);
                                } else if (count == 4) {
                                        char* ratingStr = strtok(token, ".");
                                        int rating = atoi(ratingStr);
                                        players[numPlayers].rating = rating;
                                        // categorize player based on rating
                                        if (rating >= 80 && rating <= 100) {
                                                char foldername[MAX_NAME_LEN + 5] = "best/";
                                                strcat(foldername, filename);
                                                rename(filename, foldername);
                                        } else if (rating >= 60 && rating <= 79) {
                                                char foldername[MAX_NAME_LEN + 6] = "good/";
                                                strcat(foldername, filename);
                                                rename(filename, foldername);
                                        } else if (rating >= 40 && rating <= 59) {
                                                char foldername[MAX_NAME_LEN + 7] = "average/";
                                                strcat(foldername, filename);
                                                rename(filename, foldername);
                                        } else {
                                                char foldername[MAX_NAME_LEN + 8] = "below_average/";
                                                strcat(foldername, filename);
                                                rename(filename, foldername);
                                        }
                                }
                                token = strtok(NULL, "");
                        }
                        numPlayers++;
                }
        }
        closedir(dir);
        printf("Players categorized.\n");
}

void buatTim(int bek, int gelandang, int striker) {
    int count_bek = 0, count_gelandang = 0, count_striker = 0;
    int index_bek[MAX_PLAYERS], index_gelandang[MAX_PLAYERS], index_striker[MAX_PLAYERS];
    // separate player indices for each position
    for (int i = 0; i < numPlayers; i++) {
        if (strcmp(players[i].position, "Bek") == 0) {
            index_bek[count_bek] = i;
            count_bek++;
        } else if (strcmp(players[i].position, "Gelandang") == 0) {
            index_gelandang[count_gelandang] = i;
            count_gelandang++;
        } else if (strcmp(players[i].position, "Striker") == 0) {
            index_striker[count_striker] = i;
            count_striker++;
        }
    }
    // check if there are enough players in each position
    if (count_bek < bek || count_gelandang < gelandang || count_striker < striker) {
        printf("Tidak cukup pemain untuk membentuk tim.\n");
        return;
    }
    // shuffle players index for each position
    for (int i = 0; i < count_bek; i++) {
        int j = rand() % count_bek;
        int temp = index_bek[i];
        index_bek[i] = index_bek[j];
        index_bek[j] = temp;
    }
    for (int i = 0; i < count_gelandang; i++) {
        int j = rand() % count_gelandang;
        int temp = index_gelandang[i];
        index_gelandang[i] = index_gelandang[j];
        index_gelandang[j] = temp;
    }
    for (int i = 0; i < count_striker; i++) {
        int j = rand() % count_striker;
        int temp = index_striker[i];
        index_striker[i] = index_striker[j];
        index_striker[j] = temp;
    }
    // create team
    printf("Berikut adalah susunan tim:\n");
    printf("Bek:\n");
    for (int i = 0; i < bek; i++) {
        printf("%s\n", players[index_bek[i]].name);
    }
    printf("Gelandang:\n");
    for (int i = 0; i < gelandang; i++) {
        printf("%s\n", players[index_gelandang[i]].name);
    }
    printf("Striker:\n");
    for (int i = 0; i < striker; i++) {
        printf("%s\n", players[index_striker[i]].name);
    }
}
